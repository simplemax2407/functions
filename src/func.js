const getSum = (str1, str2) => {

    if (checkStrT2(str1) || checkStrT2(str2)) {
        return false;
    }
    return (Number(str1) + Number(str2)).toString();
};

function checkStrT2(str) {
    return Array.isArray(str) || typeof(str) === 'object' || isNaN(str);
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {

    let posts = 0;
    let comments = 0;

    for (let p of listOfPosts) {

        if (p.author == authorName) {
            posts++;
        }

        if (Array.isArray(p.comments)) {
            for (let c of p.comments) {
                if (c.author == authorName) {
                    comments++;
                }
            }
        }
    }

    return 'Post:' + posts + ',comments:' + comments;
};

const tickets = (people) => {
    let coins = [100, 50, 25];
    let res = Array(coins.length).fill(0);

    for (let p of people) {
        let index = coins.findIndex(c => c === p);

        if (index < 0) {
            return 'NO';
        }
        res[index]++;
    }

    return checkT3(res, coins, 0) && checkT3(res, coins, 1) ? 'YES' : 'NO';
};

function checkT3(counts, coins, initIndex) {
    
    if (counts[initIndex] === 0) {
        return true;
    }

    let sum = 0;
    for (let i = initIndex + 1; i < coins.length; i++) {
        sum += counts[i] * coins[i];
    }

    
    return coins[initIndex] <= sum;
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
